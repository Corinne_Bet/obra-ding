#ifndef WATCHY_ObraDing_H
#define WATCHY_ObraDing_H

#include <Watchy.h>
#include "graphics.h"

// Btn definitions
#define IS_DOUBLE_TAP       (wakeupBit & ACC_INT_MASK && guiState == WATCHFACE_STATE)
#define IS_BTN_RIGHT_UP     (wakeupBit & UP_BTN_MASK && guiState == WATCHFACE_STATE)
#define IS_BTN_LEFT_UP      (wakeupBit & BACK_BTN_MASK && guiState == WATCHFACE_STATE)
#define IS_BTN_RIGHT_DOWN   (wakeupBit & DOWN_BTN_MASK && guiState == WATCHFACE_STATE)
#define EXT_INT_MASK        MENU_BTN_MASK|BACK_BTN_MASK|UP_BTN_MASK|DOWN_BTN_MASK



class ObraDing : public Watchy{
    using Watchy::Watchy;
    public:
        ObraDing();
		void drawPixel(int16_t x, int16_t y,uint16_t col);
		void drawBitmapCol(int16_t x, int16_t y, const uint8_t *bitmap, int16_t w, int16_t h, uint16_t color1);
		int getPixel(int16_t x, int16_t y, const uint8_t *bitmap);
		void drawMyRect(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color1);
		void drawWatchFace();
    void drawBitmapRotate(int xx, int yy, const uint8_t *bitmap, unsigned int fAngle, uint16_t color);
    void displayBattery();
    virtual void handleButtonPress();
    void vibrate(uint8_t times=1, uint32_t delay_time=50);
    esp_sleep_wakeup_cause_t wakeup_reason;
};

#endif
