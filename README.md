# Obra-Ding

![Pocket watch animation](https://gitlab.com/Corinne_Bet/obra-ding/-/raw/main/src/preview.gif?ref_type=heads)

## A tribute to the obra dinn video game

Obra Dinn is an awesome videogames by [dukope](https://dukope.com/) in wich you travel through time with a pocket watch.
I redesigned the watch to fit a square frame and keep the pocket watch opening and closing. Moreover, inspired by the time travel effect, the battery level appear as a dark shape filling step by step the entire screen. 

## More than an homage, an workflow inspired

Lucas Pope is a really great gamedesigner with exciting devlog ! Inspired by his workflow, First, I designed the pocketwatch inside [blender 3D software](https://www.blender.org/). Then I recreated a dither shader to match the obra dinn graphics. Finnaly i Uploaded the watch with the [arduino IDE](https://www.arduino.cc/en/software/).
