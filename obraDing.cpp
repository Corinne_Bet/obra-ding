#include "obraDing.h"

//ObraDing::ObraDing(){} //constructor

RTC_DATA_ATTR bool playAnim = true;
RTC_DATA_ATTR bool isOpened = false;

#define GREY 0x7BEF


void ObraDing::drawPixel(int16_t x, int16_t y,uint16_t col){
  switch (col){
    case GREY:
      if(y&1){
        if(x&1){
          display.drawPixel(x, y, GxEPD_BLACK);
        }else{
          display.drawPixel(x, y, GxEPD_WHITE);
        }
      }else{
        if(x&1){
          display.drawPixel(x, y, GxEPD_WHITE);
        }else{
          display.drawPixel(x, y, GxEPD_BLACK);
        }
      }
      break;
    default:
      display.drawPixel(x, y, col);
      break;
  }
}

void ObraDing::drawBitmapCol(int16_t x, int16_t y, const uint8_t *bitmap, int16_t w, int16_t h, uint16_t color1){
  int16_t i, j, byteWidth = (w + 7) / 8;
  for(j=0; j<h; j++) {
    for(i=0; i<w; i++ ) {
      if((pgm_read_byte(bitmap + j * byteWidth + i / 8) & (128 >> (i & 7)))==0) {
        drawPixel(x+i, y+j, color1);
      }
    }
  }
}

int ObraDing::getPixel(int16_t x, int16_t y, const uint8_t *bitmap){
  int16_t imageWidth = pgm_read_byte(bitmap);
  int16_t byteWidth = (imageWidth + 7) / 8;
  return (pgm_read_byte(bitmap + 2 + y * byteWidth + x / 8) & (128 >> (x & 7)));
}

void ObraDing::drawMyRect(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color1){
  for(int j=0; j<h; j++) {
    for(int i=0; i<w; i++ ) {
      drawPixel(x+i, y+j, color1);
    }
  }  
}

void ObraDing::drawBitmapRotate(int xx, int yy, const uint8_t *bitmap, unsigned int fAngle, uint16_t color=GxEPD_BLACK){

  int iWidth = pgm_read_byte(bitmap);
  int iHeight = pgm_read_byte(bitmap + 1);
  int hX = iWidth/2;
  int hY = iHeight;
  float angle = fAngle * PI / 180.0;

  int startX = -hX;
  int endX = startX + iWidth;
  int startY = -hY;
  int endY = startY + iHeight;

  // we only need to draw pixels in the circular area the image can reach from its center.
  // For simplicity, we search the bounding box of this area.
  // assumes that the vertical dimension of the image is the larger one
  assert(hX <= hY);
  int startAreaX = xx - hY;
  int startAreaY = yy - hY;
  int endAreaX = 200 - startAreaX;
  int endAreaY = 200 - startAreaY;
  for (int x = 0; x < 200; x++) {
    yield();
    for (int y = 0; y < 200; y++) {
      int ux = (x-xx) * cos(-angle) - (y-yy) * sin(-angle);
      int uy = (x-xx) * sin(-angle) + (y-yy) * cos(-angle);
      
      if(ux >= startX && ux < endX && uy >= startY && uy < endY){
        if(!getPixel(ux + hX, uy + hY, bitmap)){
          drawPixel(x, y, color);
        }
      }
    }
  }
}


void ObraDing::drawWatchFace() { //override this method to customize how the watch face looks

  int theHour = currentTime.Hour;
  int theMinute = currentTime.Minute;
  if(isOpened == false){
    //watch closed
    drawMyRect(0, 0, 200, 200, GxEPD_WHITE);
    drawBitmapCol(0, 0, CoverFrame, 200, 200, GxEPD_BLACK);
  } else {
    //watch opened
    // background
    drawMyRect(0, 0, 200, 200, GxEPD_WHITE);
    drawBitmapCol(0, 0, ClockBG, 200, 200, GxEPD_BLACK);

    // hour hand
    int hourAngle = ((theHour%12)*60 + theMinute) * 360 / 720;
    drawBitmapRotate(100,100, hourArrowMask, hourAngle, GxEPD_WHITE);
    drawBitmapRotate(100,100, hourArrow, hourAngle, GxEPD_BLACK);

    // minute hand
    drawBitmapRotate(100,100, minArrowMask, theMinute * 6, GxEPD_WHITE);
    drawBitmapRotate(100,100, minArrow, theMinute * 6, GxEPD_BLACK);

    // dot in middle
    drawBitmapCol(92, 92, dot, 16, 16, GxEPD_WHITE);
    drawBitmapCol(92, 92, dot2, 16, 16, GxEPD_BLACK);
  }
  displayBattery();
}


////////////////////BUTTONS///////////////////////////////////

void ObraDing::handleButtonPress() {
  uint64_t wakeupBit = esp_sleep_get_ext1_wakeup_status();
  if (IS_DOUBLE_TAP) {
    while (!sensor.getINT()) {
      // Wait until interrupt is cleared.
      // Otherwise it will fire again and again.
    }

    // To be defined in the watch face what we want exactly
    // to do. Therefore, no return;
  }


  if (IS_BTN_RIGHT_UP) {
    RTC.read(currentTime);
    int theHour = currentTime.Hour;
    int theMinute = currentTime.Minute;
    vibrate();
    //playAnim = 1;
    if(isOpened == false){
      display.setPartialWindow(0, 0, 200, 200);
      display.firstPage();
        do
        {
          drawBitmapCol(0, 0, FirstFrame, 200, 200, GxEPD_BLACK);
          displayBattery();
          //delay(680);
        }
        while (display.nextPage());
      
    //watch opened
    // background
      display.setPartialWindow(0, 0, 200, 200);
      display.firstPage();
        do
        {   
          drawMyRect(0, 0, 200, 200, GxEPD_WHITE);
          drawBitmapCol(0, 0, ClockBG, 200, 200, GxEPD_BLACK);

          // hour hand
          int hourAngle = ((theHour%12)*60 + theMinute) * 360 / 720;
          drawBitmapRotate(100,100, hourArrowMask, hourAngle, GxEPD_WHITE);
          drawBitmapRotate(100,100, hourArrow, hourAngle, GxEPD_BLACK);

          // minute hand
          drawBitmapRotate(100,100, minArrowMask, theMinute * 6, GxEPD_WHITE);
          drawBitmapRotate(100,100, minArrow, theMinute * 6, GxEPD_BLACK);

          // dot in middle
          drawBitmapCol(92, 92, dot, 16, 16, GxEPD_WHITE);
          drawBitmapCol(92, 92, dot2, 16, 16, GxEPD_BLACK);

          drawMyRect(0, 74, 200, 126, GxEPD_WHITE);
          drawBitmapCol(0, 74, SecondFrame, 200, 126, GxEPD_BLACK);  
          displayBattery();
  
          //delay(680);
        }
        while (display.nextPage());
    isOpened = true;
    showWatchFace(true);
    return;
    };
    if(isOpened == true){
      display.setPartialWindow(0, 74, 200, 126);
      display.firstPage();
        do
        {
          drawBitmapCol(0, 74, SecondFrame, 200, 126, GxEPD_BLACK);
          displayBattery();
          //delay(680);
        }
        while (display.nextPage());
      display.setPartialWindow(0, 0, 200, 200);
      display.firstPage();
        do
        {
          drawBitmapCol(0, 0, FirstFrame, 200, 200, GxEPD_BLACK);
          displayBattery();
          //delay(680);
        }
        while (display.nextPage());
    };
    isOpened = false;
    //playAnim = true;
    showWatchFace(true);
    return;
  }

  if (IS_BTN_RIGHT_DOWN) {
    //return
  }

  Watchy::handleButtonPress();
}

void ObraDing::vibrate(uint8_t times, uint32_t delay_time) {
  // Ensure that no false positive double tap is produced
  sensor.enableFeature(BMA423_WAKEUP, false);

  pinMode(VIB_MOTOR_PIN, OUTPUT);
  for (uint8_t i = 0; i < times; i++) {
    delay(delay_time);
    digitalWrite(VIB_MOTOR_PIN, true);
    delay(delay_time);
    digitalWrite(VIB_MOTOR_PIN, false);
  }

  sensor.enableFeature(BMA423_WAKEUP, true);
}

void ObraDing::displayBattery(){
  float roundBAT = getBatteryVoltage() * 100;
  float VBAT = map(roundBAT,375,382,0,142);
  for(int i = 142 ; i >= VBAT; i-- ){
    display.drawCircle(100,100,i,GxEPD_BLACK);
  };

};

